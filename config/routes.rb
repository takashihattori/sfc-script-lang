Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'
  devise_for :users
  resources :scores
  resources :courses, only: [ :index, :show ]
  resources :faculties, only: [ :show ]
  resources :students do
    collection do
      get 'search'
      post 'search', action: :result
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
